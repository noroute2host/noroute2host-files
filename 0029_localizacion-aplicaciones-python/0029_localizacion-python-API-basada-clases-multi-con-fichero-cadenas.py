import definicion_cadenas as defstr

# Funcion para imprimir cadenas
def imprimir_cadenas():
    print(defstr.STR_CADENAS_LOCALIZABLES)
    print(defstr.STR_HOLA)
    print(defstr.STR_ADIOS)
    print(defstr.STR_NUEVA_CADENA_LOCALIZABLE)

if __name__ == '__main__':
    imprimir_cadenas()
