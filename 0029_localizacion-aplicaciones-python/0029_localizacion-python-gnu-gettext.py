import gettext
_ = gettext.gettext

def imprimir_cadenas():
    print(_("Somos cadenas localizables"))
    print(_("Hola noroute2host"))
    print(_("Adios noroute2host"))

if __name__ == '__main__':
    imprimir_cadenas()
