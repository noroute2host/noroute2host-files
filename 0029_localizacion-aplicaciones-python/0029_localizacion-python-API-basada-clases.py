import gettext

appname = "noroute2host"
localedir = "locales"

translations = gettext.translation(appname, localedir, fallback=True, languages=["en", "es"])
translations.install()

def imprimir_cadenas():
    print(_("Somos cadenas localizables"))
    print(_("Hola noroute2host"))
    print(_("Adios noroute2host"))

if __name__ == '__main__':
    imprimir_cadenas()
