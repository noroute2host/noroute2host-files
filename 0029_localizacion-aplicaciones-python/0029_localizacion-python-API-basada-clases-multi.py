import gettext

appname = "noroute2host"
localedir = "locales"

# Funcion para seleccionar el idioma
def preguntar_idioma():
    print("Seleccion de Idioma")
    response = ''
    while response not in {"esp", "eng"}:
        response = input("Por favor introduce esp o eng: ").lower()
    if response == "eng":
        translations = gettext.translation(appname, localedir, fallback=True, languages=["en"])
    else:
        translations = gettext.translation(appname, localedir, fallback=True, languages=["es"])
    translations.install()
        
# Funcion para imprimir cadenas
def imprimir_cadenas():
    print(_("Somos cadenas localizables"))
    print(_("Hola noroute2host"))
    print(_("Adios noroute2host"))
    print(_("Soy una nueva cadena localizable"))

if __name__ == '__main__':
    preguntar_idioma()
    imprimir_cadenas()
