import gettext

appname = "noroute2host-defstr"
localedir = "locales"

# Funcion para seleccionar el idioma
def preguntar_idioma():
    print("Seleccion de Idioma")
    response = ''
    while response not in {"esp", "eng"}:
        response = input("Por favor introduce esp o eng: ").lower()
    if response == "eng":
        translations = gettext.translation(appname, localedir, fallback=True, languages=["en"])
    else:
        translations = gettext.translation(appname, localedir, fallback=True, languages=["es"])
    translations.install()

# Se instalan las traducciones
preguntar_idioma()

# Definicion de cadenas localizables

STR_CADENAS_LOCALIZABLES = _("somos-cadenas-localizables")
STR_HOLA = _("hola-noroute2host")
STR_ADIOS = _("adios-noroute2host")
STR_NUEVA_CADENA_LOCALIZABLE = _("nueva-cadena-localizable")
