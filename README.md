# noroute2host files

Repositorio público para todos los fragmentos de código, scripts, ejemplos o cualquier tipo de fichero relacionado con la publicaciones de la web <https://noroute2host.com>

## Tabla de contenidos

[TOC]

## Acerca del proyecto noroute2host

noroute2host nace por dos motivaciones, por un lado la necesidad de descubrir nuevas tecnologías libres y por otro, compartirlas con todo aquel al que le parezcan interesantes.

Cierto, a estas alturas ya existe stackoverflow. Y muy bien que está, pero no todo es eso. Por un lado la puesta en marcha de este sitio me ha permitido aprender o afianzar tecnologías. Pero además, y pese a que soy de los que casi siempre prefiere buscar la documentación en inglés puesto que después hace más fácil identificar lo que estás buscando, es cierto que el contenido en español en temas técnicos es menor y me apetecía hacer algo al respecto.

Evidentemente esto no tiene nada que ver con stackoverflow o UNIX Stack Exchange. noroute2host es simplemente un rinconcito de información en la que aparecerá lo que me pique en cada momento, lo que me parezca interesante o simplemente lo que me apetezca. Para que nos vamos a engañar…

También reconozco que el proyecto tiene algo de nostalgia puesto que ya hubo un noroute2host.com de la mano de amigos que admiro y aprecio. Así que esto es como su reencarnación. Si tienes curiosidad por ver como fue aquello siempre nos quedará la [WayBack Machine](http://web.archive.org/web/20120416213935/http://www.noroute2host.com/) de archive.org

## Como estar al dia de lo que pasa en noroute2host

Si quieres estar al tanto de lo que pasa en noroute2host puedes:

- Visitar la web: <https://noroute2host.com>
- Comenzar a seguirnos en nuestras redes sociales:
    - [Mastodon](https://mastodon.social/@noroute2host)
    - [Twitter](https://twitter.com/noroute2hostcom)

- O simplemente suscribirte a nuestro [RSS](http://noroute2host.com/feeds/all.atom.xml)

## Acerca del repositorio noroute2host-files

Este repositorio nace con la idea de publicar todos los scripts, fragmentos de código, utilidades o ejemplos que aparezcan en los diferentes artículos.

Así estarán, facilmente disponibles para todo el mundo y podrán mejorarse, corregirse o adaptarse.

Eso si, al ser un compendio de ejemplos, scripts, etc, habrá un mix de tecnologías diferentes según el artículo relacionado. Así que la única manera que se me ha ocurrido de organizar el repositorio es creando una carpeta para el contenido de cada artículo que tenga ficheros en este repositorio.

## Instalación

Usa git clone para clonar este repositorio en tu equipo.

```bash
git clone https://gitlab.com/noroute2host/noroute2host-files
```

## Contribuir

Colaborando con el código 💻

- 🍴 Haz un fork del repo [aquí](https://gitlab.com/noroute2host/noroute2host-files/-/forks/new)
- 🔨 Dale caña
- 😊 Añadete como contribuidor
- 🔧 Haz una petición de pull [aquí](https://gitlab.com/noroute2host/noroute2host-files/-/merge_requests)

Las peticiones de pull son bienvenidas. Para cambios mayores, por favor, abre un issue primero para discutir el cambio.

O simplemente creando un issue 😊

- 😯 Creando un issue [aquí](https://gitlab.com/noroute2host/noroute2host-files/-/issues)

## Licencia

Los ficheros de este repositorio estan licenciados bajo la licencia **GNU General Public License v3.0**. Para más detalles se puede consultar el fichero LICENSE.txt de este repositorio.