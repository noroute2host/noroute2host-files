#!/bin/bash

######################################################################
# @author      : @AdriMcGrady para https://noroute2host.com
# @file        : 0030_enviar_mensaje_telegram
# @created     : domingo dic 09, 2024 17:00:38 CEST
#
# @description : Script para enviar mensajes de telegram desde un 
#                script en bash.
######################################################################

# Definicion de colores
RED='\e[01;31m'
GREEN='\e[01;32m'
BLUE='\e[01;34m'
AMARILLO='\e[01;93m'
NC='\e[01;0m' # No Color

# Variables
URL_API_TELEGRAM=""
DEBUG="false"
CONF_FILE=""
TOKEN=""
CHAT_ID=""
MENSAJE=""
CURL=""

function uso() {
	echo -e "${RED}Este script se puede ejecutar de dos formas.${NC}" 1>&2
	echo -e "${RED}1. Usando un fichero de configuracion${NC}" 1>&2
	echo -e "${RED}  Uso: $0 -f \"ruta/config/file.conf\" -m \"Este es un mensaje de Prueba\"${NC}" 1>&2 
	echo -e "${RED}    Ejemplo de fichero de configuracion:${NC}" 1>&2 
	echo -e "${RED}      TOKEN="TOKEN_SUPERSECRETO_DEL_BOT"${NC}" 1>&2 
	echo -e "${RED}      CHAT_ID="CHAT_ID_SUPERSECRETO_DEL_BOT"${NC}" 1>&2 
	echo -e "${RED} 2. Pasando directamente el token y el chatid${NC}" 1>&2
	echo -e "${RED}  Uso: $0 -t \"TU_TOKEN\" -c \"CHAT_ID_DONDE_MANDAR_MENSAJE\" -m \"Este es un mensaje de prueba\"${NC}" 1>&2
}

function salidaError() {
	uso
	exit 1
}

function imprimirError() {
	echo -e "${RED}ERROR: $1${NC}"
}

# Parametros del script 
while getopts ":dhf:t:c:m:" options; do
	case "${options}" in
		d)
                        DEBUG=true
                        ;;
		h)
			# Ayuda
			uso
			exit
			;;
		f)
			# Fichero de configuracion 
			CONF_FILE=${OPTARG}
			;;
		t)
			# Token. Solo se usara si no hay config file 
			TOKEN=${OPTARG}
			;;
		c)
			# ChatID. Solo se usara si no hay config file
			CHAT_ID=${OPTARG}
			;;
		m)
			# Mensaje. Obligatorio. 
			MENSAJE=${OPTARG}
			;;
		*)
			salidaError
			;;
	esac
done

# Comprobacion de parametros obligatorios
# Si no hay mensaje error.
if [ -z "$MENSAJE" ]; then
	salidaError
else
	# Si no hay fichero de config token y chatid tienen que estar definidos
	if [ -z "$CONF_FILE" ]; then
		if [ -z "$TOKEN" ] || [ -z "$CHAT_ID" ]; then
			salidaError
		fi
	else
		# Si hay fichero de config tiene que existir
		if [ ! -f "$CONF_FILE" ]; then
			imprimirError "El fichero de configuracion $CONF_FILE no existe"
			exit 3
		else
			# Cargar fichero de configuracion
			source $CONF_FILE
			# Comprobar variables cargadas
			if [ -z "$TOKEN" ] || [ -z "$CHAT_ID" ]; then
				imprimirError "El fichero de configuracion no contenia las variables requeridas"
			fi
		fi
	fi
fi

# Comprobacion dependencia curl
function buscaCurl() {
	CURL=$(which curl)
	if [ -z "$CURL" ]; then
		imprimirError "curl no esta disponible"
		exit 4
	fi

}

# La funcion que realmente envia el mensaje
function enviarMensaje() {
	buscaCurl
	URL="https://api.telegram.org/bot$TOKEN/sendMessage"
	if $DEBUG; then
		$CURL -s -X POST $URL -d chat_id=$CHAT_ID -d text="$MENSAJE"
	else
		# Si no esta habilitado el debug, se envia la respuesta a /dev/null
		$CURL -o /dev/null -s -X POST $URL -d chat_id=$CHAT_ID -d text="$MENSAJE"
	fi
}

# Cuerpo del script. Solo enviar el mensaje
enviarMensaje
if [ $? -eq 0 ] && $DEBUG; then
       echo -e "\n${GREEN}Mensaje enviado correctamente${NC}"
fi       
