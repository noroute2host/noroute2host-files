#!/bin/bash

### Script para ejecutar comprobar si un listado de servidores responden a ping.
### Se puede usar pasando la salida del comando prips por pipe o directamente
### indicando un fichero con la lista de las ips que se quieren probar.
### @AdriMcGrady para https://noroute2host.com
 
# Trap para salir con CTRL-C
trap "exit" INT

# Definicion de colores
RED='\e[01;31m'
GREEN='\e[01;32m'
BLUE='\e[01;34m'
AMARILLO='\e[01;93m'
NC='\e[01;0m' # No Color

# Variables
ENTRADAPIPE=false
LISTA_IPS=""
FICHERO_IPS=""
REPORTE=false
REPORTE_POSITIVOS=""
REPORTE_NEGATIVOS=""
DEBUG=false

function uso() {
	echo "El script de multiping puede usarse de dos modos diferentes:" 1>&2
	echo "MODO 1: Recibiendo una lista de IPs via pipe. Por ejemplo con prips: prips RED/MASCARA | $0 [-r] [-d]" 1>&2
	echo "MODO 2: Recibiendo un fichero con la lista de IPS. Por ejemplo: | $0 -f FICHERO-IPS [-r] [-d]" 1>&2
}

function salidaError() {
	uso
	exit 1
}

function imprimirDebug() {
        if [ "$DEBUG" = true ]; then
                echo -e "${AMARILLO}DEBUG: $1${NC}"
        fi
}

while getopts ":df:hr" options; do
	case "${options}" in
		d)
                        DEBUG=true
                        ;;
		f)
			FICHERO_IPS=${OPTARG}
			if [ -z "$FICHERO_IPS" ]; then
				salidaError
			fi	
			# Si hay datos por pipe se ignora el fichero de entrada
			if [ ! -p /dev/stdin ]; then
				# Comprobar si el fichero existe
				if [ -f $FICHERO_IPS ]; then
                                	LISTA_IPS=$(cat "$FICHERO_IPS"|egrep -v '^#')
                        	else
					echo -e "${AMARILLO}El fichero $FICHERO_IPS no existe${NC}"
					salidaError
                        	fi
			fi
			;;
		r)
			REPORTE=true
			;;
		h)
			uso
			exit
			;;
		*)
			salidaError
			;;
	esac
done

function capturarEntrada() {
	# Comprobar si se ha pasado información por stdin y capturar ips
	if [ -p /dev/stdin ]; then
		ENTRADAPIPE=true
		imprimirDebug "Se han pasado datos via pipe"
		LISTA_IPS=$(cat) 
	else
		# Ahora comprobamos que se haya pasado el nombre del fichero por parametros y que exista
		if [ -z "$FICHERO_IPS" ]; then
			imprimirDebug "No se han pasado datos ni por STDIN ni por fichero de parametros"
		fi
	fi
}

function multiping() {
	printf "${BLUE}`date` - Comienzo de la prueba.${NC}\n"

	for ip in $LISTA_IPS
	do
        	echo "$ip: Probando ping..."
        	ping -q -c 1 $ip 2>&1 >/dev/null
        	if [ $? -ne 0 ]; then
                	printf "${RED}    $servidor NO responde a ping${NC}\n"
			if [ "$REPORTE" = true ] ; then
				REPORTE_NEGATIVOS+=" $ip"
			fi
        	else
                	printf "${GREEN}    $servidor SI responde a ping${NC}\n"
			if [ "$REPORTE" = true ] ; then
                        	REPORTE_POSITIVOS+=" $ip"
                	fi
        	fi
	done

	printf "${BLUE}`date` - Fin de la prueba.${NC}\n"
}

function resumen(){
	printf "${BLUE}`date` - Reporte de Hosts que SI responden a PING.${NC}\n"
	for i in $REPORTE_POSITIVOS
	do
		printf "${GREEN}    ${i}${NC}\n"
	done

	printf "${BLUE}`date` - Reporte de Hosts que NO responden a PING.${NC}\n"
	for i in $REPORTE_NEGATIVOS
	do
        	printf "${RED}    ${i}${NC}\n"
	done
}

capturarEntrada $1
multiping
if [ "$REPORTE" = true ] ; then
	resumen
fi
