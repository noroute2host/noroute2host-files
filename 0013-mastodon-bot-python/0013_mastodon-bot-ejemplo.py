# Importar la clase necesaria desde Mastodon.py
from mastodon import Mastodon

# Crear la instancia de la API. El token debe ser reemplazado aqui. Y la instancia tambien en api_base_url
mastodon = Mastodon(
    access_token = 'TuTokenSuperSecreto',
    api_base_url = 'https://TuInstanciaMastodon'
)

# Tootear. Guardo la salida en la variable toot para poder responder despues.
toot=mastodon.toot('Mi primer toot usando la API')

# Tootear en modo respuesta. Para identificar a que toot responder podria usar directamente la variable toot o solo el id que se guarda en toot['id']
mastodon.status_post("Y ahora me estoy respondiendo a mi mismo", in_reply_to_id = toot)