#!/bin/bash

### Script para, a partir de un directorio con imágenes (todas en el mismo formato),
### generar los recursos "drawable" para diferentes dpi para el desaroollo de una 
### app de Android. Además permite la copia de los recursos directamente al proyecto
### de la app en Android Studio.
### @AdriMcGrady para https://noroute2host.com
 
# Definicion de colores
RED='\e[01;31m'
GREEN='\e[01;32m'
BLUE='\e[01;34m'
AMARILLO='\e[01;93m'
NC='\e[01;0m' # No Color

# Variables
DEBUG=false
DIR_ORIGEN=""
DIR_SALIDA=""
DIR_APP=""
FORMATO_ORIGINAL=""
FORMATO_SALIDA=""
BINIM=""

function uso() {
	echo "Uso: $0 -i Dir_Imagenes_Base -o Dir_Salida -f Formato_Imgs_Origen -s Formato_Imgs_Salida [-c Dir_res_Proyecto_app]" 1>&2
	echo "Ejemplo de uso: ./0018_generar_drawables_dpi.sh -i /home/usuario/0018_imagemagick-android/imgs/originales -o /home/usuario/imgs -f xcf -s png -c /home/usuario/StudioProjects/miapp/app/src/main/res" 1>&2
}

function salidaError() {
	uso
	exit 1
}

function imprimirDebug() {
        if [ "$DEBUG" = true ]; then
                echo -e "${AMARILLO}DEBUG: $1${NC}"
        fi
}

function imprimirError() {
	echo -e "${RED}ERROR: $1${NC}"
}

while getopts ":dhi:o:c:f:s:" options; do
	case "${options}" in
		d)
                        DEBUG=true
                        ;;
		h)
			# Ayuda
			uso
			exit
			;;
		i)
			# Directorio de origen de las imagenes. Todas en el mismo formato.
			DIR_ORIGEN=${OPTARG}
			;;
		o)
			# Directorio de salida padre para los recursos drawable.
			DIR_SALIDA=${OPTARG}
			;;
		c)
			# Directorio de Proyecto de la App para copiar. Si no se define, no se copiara.
			# Si se define se comprobara si exientes las subcarpetas dentro de drawable.
			DIR_APP=${OPTARG}
			;;
		f)
			# Formato de imagenes de entrada. Por ejemplo XCF.
			FORMATO_ORIGINAL=${OPTARG}
			;;
		s)
			# Formato de imagenes e salida. Por ejemplo PNG.
			FORMATO_SALIDA=${OPTARG}
			;;
		*)
			salidaError
			;;
	esac
done

# Comprobacion de parametros obligatorios
if [ -z "$DIR_ORIGEN" ] || [ -z "$DIR_SALIDA" ] || [ -z "$FORMATO_ORIGINAL" ] || [ -z "$FORMATO_SALIDA" ]; then
	salidaError

	# Comprobacion de existencia de directorios
else
	if [ ! -d "$DIR_ORIGEN" ] || [ ! -d "$DIR_SALIDA" ]; then
		imprimirError "Alguno de los directorios proporcionados no existe"
		exit 3
	fi
fi

# Comprobacion de parametros opcionales
if [ -n "$DIR_APP" ]; then	
	if [ ! -d "$DIR_APP" ]; then
		imprimirError "El directorio res/drawable de proyecto proporcionado no existe"
		exit 4
	fi
fi


# Funcion para comprobar si imagemagic esta en el path y ver si es magick o convert.
function getPathIM() {
	echo -e "${GREEN}  Comprobando la instalación de ImageMagick...${NC}"
	if which magick > /dev/null ; then
		BINIM=$(which magick)
		imprimirDebug "Binario ImageMagick: $BINIM"
	else
		if which convert > /dev/null ; then
			BINIM=$(which convert)
                	imprimirDebug "Binario ImageMagick: $BINIM"
		else
			imprimirError "ImageMagick NO esta instalado o no esta en el PATH"
			exit 2
		fi
	fi
}


# Funcion para generar los recursos drawables a partir de las imagenes del directorio pasado con -i
# Las imagenes se redimensionaran en un porcentaje pensando que el tamaño de la imagen original es el de XXXHDPI.

function generarDrawables() {

	echo -e "${GREEN}  Generando recursos...${NC}"

	# Crear directorios si no existen previamente
	for i in xxxhdpi xxhdpi xhdpi hdpi mdpi ldpi; do
		mkdir -p $DIR_SALIDA/$i
	done

	# Se activa el modo NO case sensitive para pillar todas las extensiones en mayusculas o minusculas
	shopt -s nocaseglob

	#xxxhdpi
	imprimirDebug "$BINIM mogrify -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/xxxhdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL"	
	$BINIM mogrify -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/xxxhdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL
	#xxhdpi
	imprimirDebug "$BINIM mogrify -resize 75% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/xxhdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL"	
	$BINIM mogrify -resize 75% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/xxhdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL
	#xhdpi
        imprimirDebug "$BINIM mogrify -resize 50% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/xhdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL"
        $BINIM mogrify -resize 50% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/xhdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL
	#hdpi - Ver que pasa con el porcentaje
        imprimirDebug "$BINIM mogrify -resize 37.5% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/hdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL"
        $BINIM mogrify -resize 37.5% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/hdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL
	#mdpi
        imprimirDebug "$BINIM mogrify -resize 25% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/mdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL"
        $BINIM mogrify -resize 25% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/mdpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL
	#ldpi - Ver que pasa con el porcentaje
        imprimirDebug "$BINIM mogrify -resize 18.75% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/ldpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL"
        $BINIM mogrify -resize 18.75% -format $FORMATO_SALIDA -flatten -path $DIR_SALIDA/ldpi $DIR_ORIGEN/*.$FORMATO_ORIGINAL

	# Se vuele al modo case sensitive
	shopt -u nocaseglob
}

# Funcion para copiar a proyecto de Android las imagenes en las correspondientes carpetas

function copiarAProyecto() {
	
	echo -e "${GREEN}  Copiando al proyecto...${NC}"

	# Se activa el modo NO case sensitive para pillar todas las extensiones en mayusculas o minusculas
        shopt -s nocaseglob

	for i in xxxhdpi xxhdpi xhdpi hdpi mdpi ldpi; do
		mkdir -p $DIR_APP/drawable-$i
		imprimirDebug "cp -a $DIR_SALIDA/$i/*.$FORMATO_SALIDA $DIR_APP/drawable-$i"
		cp -a $DIR_SALIDA/$i/*.$FORMATO_SALIDA $DIR_APP/drawable-$i
	done

	# Se vuele al modo case sensitive
        shopt -u nocaseglob
	
}

# Cuerpo principal del script

echo -e "${GREEN}Comienzo de la ejecución.${NC}"
getPathIM
generarDrawables
if [ -n "$DIR_APP" ]; then
	copiarAProyecto
fi
echo -e "${GREEN}Fin de la ejecución.${NC}"
