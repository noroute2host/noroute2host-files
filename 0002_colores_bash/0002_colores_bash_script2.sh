#!/bin/bash

ROJO="\e[;31m"
CELESTE="\e[;36m"
NOCOLOR="\e[0m"

SEGUNDOS=""

for i in {1..5}; do
    SEGUNDOS=$(date +%S)
    if (( $SEGUNDOS % 2 )); then
        echo -e "${CELESTE}$SEGUNDOS es número PAR"
    else
        echo -e "${ROJO}$SEGUNDOS es número IMPAR"
    fi
    sleep 3
done
